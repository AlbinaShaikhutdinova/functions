const getSum = (str1, str2) => {
  if (!isString(str1) || !isString(str2)) {
    return false;
  }
  if (makeNumber(str1).length !== str1.length || makeNumber(str2).length !== str2.length) {
    return false;
  }
  let res = '';
  const len = str1.length > str2.length ? str1.length : str2.length;
  for (let i = 0; i < len; i++) {
    let first = 0;
    let second = 0;
    if (str1[i]) {
      first = str1[i];
    }
    if (str2[i]) {
      second = str2[i];
    }
    const sum = Number(first) + Number(second);
    res += sum;
  }
  return res;
};

function isString(x) {
  return Object.prototype.toString.call(x) === '[object String]';
}

function makeNumber(string) {
  const nums = string.match(/\d/g);
  return nums ? nums.join('') : '';
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;
  listOfPosts.forEach((post) => {
    if (post.author === authorName) {
      posts++;
    }
    if (post.comments) {
      post.comments.forEach((comment) => {
        if (comment.author === authorName) {
          comments++;
        }
      });
    }
  });
  return `Post:${posts},comments:${comments}`;
};

const tickets = (people) => {
  let twentyFive = 0;
  let fifty = 0;
  let hundred = 0;
  let answer = 'YES';
  for (let cash of people) {
    if (cash == 25) {
      twentyFive++;
    } else if (cash == 50) {
      if (twentyFive > 0) {
        twentyFive--;
        fifty++;
      } else {
        answer = 'NO';
        break;
      }
    } else if (cash == 100) {
      if (twentyFive > 2) {
        twentyFive -= 3;
        hundred++;
      } else if (fifty > 0 && twentyFive > 0) {
        fifty--;
        twentyFive--;
        hundred++;
      } else {
        answer = 'NO';
        break;
      }
    }
  }
  return answer;
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
